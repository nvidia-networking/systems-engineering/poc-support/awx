# AWX
![Logo](https://miro.medium.com/max/1042/1*g-L5zDf10Hitj9XMlNNFug.png)


## Introduction
The lab consists in a EVPN architecture based on SPINE LEAF topology and AWX to drive Ansible operations. 

It’s mandatory to configure SSH access to OOB server from SERVICES panel in AIR interface, otherwise it won’t be possible to get AWX GUI.

## Access the web interface
Once the lab is running, we can configure an SSH port forwarding on our local computer to reach the WEB UI of AWX:

We must include in the above command the information regarding the worker node available in the AIR interface (bottom right):
![image info](img/worker.png)

`ssh -L 8043:awx:8043 -p <EXT_PORT> cumulus@<WORKER>`

Now that the tunnel is up, we can use our local browser to access AWX WEB GUI:
- https://localhost:8043
- AWX Login: admin/admin


## Execute the ping role
AXW interface is organized with a left bar that provides access to each configuration area of the tool.

Firstly, we can try a basic task, just to ensure that the lab is running smoothly. 

On the left navigate to Views section, then select Jobs: from the list click on the small rocket icon under Actions column to re-run a task (Relaunch Job). For this test use a test named PING from the list.

The Status Successful in Jobs view means that task execution was successfully, and no issue was encountered.

## Verify hosts list
As mentioned at the beginning of this guide, the lab underneath this simulation is a standard CLOS topology, that consists in:
![image info](img/fabric.png)

Let’s check the hosts list inside AWX:
- Click on the Resources section
- Click on Hosts

All the hosts included in this list belong to an Inventories which is a collection of hosts against which jobs may be launched, the same as an Ansible inventory file. 

Here we can easily enable / disable a host, to choose if a Job should run against it or not.

Let’s try to turn off leaf01 from the list, clicking on the button in the Actions column.

Now it’s time to go back to the Jobs, in the Views section: using the Rocket icon in the Actions column, Relaunch Job PING at the top of the list.

At the top of the Output section, there is a Search Box, please type leaf then press Search:

```
[DEPRECATION WARNING]: Distribution debian 10.9 on host leaf02 should use 
leaf02 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
[DEPRECATION WARNING]: Distribution debian 10.9 on host leaf03 should use 
leaf03 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
[DEPRECATION WARNING]: Distribution debian 10.9 on host leaf04 should use 
leaf04 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

As expected leaf01 is not present in the list, now we can enable it again in the Hosts section and run the Job another time, then search for it in the output using the Search Box:
```
[DEPRECATION WARNING]: Distribution debian 10.10 on host leaf01 should use 
leaf01 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```
To run Jobs against Hosts, Credentials are needed: please select Credentials in the Resources section on the left.

Credentials are used by AWX for:
-   Authentication on target Hosts when you run a Job
-	Perform synchronization with an Inventory

## Dry run EVPN L2 Extension configuration
Select Templates from the Resources section: each Template gathers all settings needed to run an Ansible task.
- Clicking on the CLDEMO2-LE-Extension-Deploy, we can access all the configuration for this Template, from the Details section look at:
    - Inventory: 	EVPN_L2ONLY (as mentioned in the previous section an Inventory is a collection of Hosts) 
    - Playbook: 	playbook/deploy.yml (this is the playbook selected from the Project CLDEMO2 - Golden Turtle configured as a GIT source)

Templates are a useful and powerful tool, that can also provide advanced functionalities as Schedules and the option to import a 
Notification policy (try to navigate through the various sections of the Top menu).

Now is time to play with Templates: 
- Click on the Templates in the Resources section and click on the Copy Template actions from the Actions column, on the right of CLDEMO2-LE-Extension-Deploy: a new copy of the template is now present in the list with the timestamp of copy action attached to the Name.
- Now click on the edit action on the right of the copied Template: on the Job type select Check then Save this change.
- Check option is needed to verify playbook syntax, test environment setup and report problems. Without impact

Once saved click on the Launch Template action on the right of copied Template (the Rocket icon): details about the execution of this dry run are available in the Output 

## Create additional templates from scratch
Now it’s time for create a new template for EVPN Centralized scenario.
There are 3 main steps:
-   Create / Import the Inventory File - for EVPN Centralized
-   Create a Template to “deploy” the EVPN Centralized architecture
-   Deploy the configurations by launching the newly created template
Create / Import EVPN Centralized Inventory
Navigate to the Resources section of the AWX GUI and click on Inventories.

![image info](img/inventory.png)

Create (“Add”) a new inventory and fill out the following information.
-   Name: EVPN_CENTRALIZED

Click Save
The page will refresh with new tabs available at the top of the section.
Click on Sources
Click the Add button and fill out the form with the following details.
-   Name: EVPN_CENTRALIZED
-   Source: Sourced from a Project
-   Project: CLDEMO2 - Golden Turtle
-   Inventory file: inventories/evpn_centralized/hosts

Click Save
On the refreshed page click Sync to import the host inventory.

Now in the Resources / Hosts section additional entries are listed and belong to EVPN_CENTRALIZED Inventory recently added.

Create a Template for EVPN Centralized deploy.yml 

Navigate to the Resources section of the AWX GUI and click on Templates

![image info](img/templates.png)

Click the Add button on the refreshed page and fill out the form with the following information.

-   Name: CLDEMO2-EVPN-Centralized-Deploy
-   Job Type: Run
-   Inventory: EVPN_CENTRALIZED
-   Project: CLDEMO2 - Golden Turtle
-   Execution Environment (optional): CLDEMO2-GT-AWX-EE-Ansible-2.10
-   Playbook: playbooks/deploy.yml
-   Credentials: cldemo2
-   Skip Tags: tacacs_client, tacacs_server

Click Save

Deploy the configurations by launching the newly created template

Now in the Resources section, in Templates list, we can launch the template, clicking on the Rocket icon on the right of CLDEMO2-EVPN-Centralized-Deploy.

Check the output results.

Now we can easily connect to border01 to test deployed scenario:
-   Access to border01 clicking on the host in the AIR GUI or using ssh from the OOB Server
-   Run the following command on border01: `net show interfaces`
-   The list of interfaces confirms that the applied configuration changed after the deploy of CLDEMO2-EVPN-Centralized-Deploy

We strongly encourage you to also try the other labs available in the marketplace, for a deeper understanding of the different EVPN deployment models:
![image info](img/marketplace.png)



## Procedure for manual deployment

### Clone the repo

`git clone https://gitlab.com/nvidia-networking/systems-engineering/poc-support/awx.git`

### Review the script
Change login/e-mail/password

###  Launch the install script
`./remote_awx.bash`

Installation is about 20min:
```
real	21m43.688s
user	0m0.250s
sys	0m0.201s```
```

