#!/bin/bash

USERNAME="julien"
EMAIL="toto@toto.com"
PASSWORD="julien"

AWX_VERSION="19.2.2"
AWX_EE_VERSION="0.6.0"
DOCKER_VERSION="20.10"
ANSIBLE_VERSION="2.9.26"
DEST="awx"

check_func()
{
	echo "return: " $1
}

# Manual validation of ssh fingerprint
ssh ${DEST} "uname -a"

echo "[+] resize vda" ; 
ssh ${DEST} "sudo growpart /dev/vda 1"
check_func $?

echo "[+] resize fs"
ssh ${DEST} "sudo resize2fs /dev/vda1"
check_func $?

if ssh ${DEST} '[ ! -d awx ]'; then
	echo "[+] - Cloning repo"
	ssh ${DEST} "git clone -b ${AWX_VERSION} https://github.com/ansible/awx.git"
else
	echo "[+] - Repo found"
fi

# Docker install
ssh ${DEST} "dpkg -l|grep docker-ce|grep ${DOCKER_VERSION} > /dev/null"
if [ $? -eq 0 ] ; then
	echo "[+] - Docker ${DOCKER_VERSION} already installed"
else
	echo "[+] - Docker ${DOCKER_VERSION} required and must be installed"
	ssh ${DEST} "curl -fsSL https://get.docker.com -o get-docker.sh"
	ssh ${DEST} "sudo sh get-docker.sh" > /dev/null
	check_func $?
	ssh ${DEST} "sudo usermod -aG docker cumulus"
	check_func $?
fi

# Compose install
if ssh ${DEST} '[ ! -f "/usr/local/bin/docker-compose" ]'; then
	ssh ${DEST} "sudo curl -L 'https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)' -o /usr/local/bin/docker-compose"
	ssh ${DEST} "sudo chmod +x /usr/local/bin/docker-compose"
	check_func $?
else
	echo "[+] - Docker compose already installed"
fi

# install ansible
echo "[+] Ansible installation" 
ssh ${DEST} "sudo pip3 install ansible==${ANSIBLE_VERSION} --upgrade" > /dev/null
check_func $?

# install AWX
echo "[+] Build container - about 5min" 
ssh ${DEST} "cd awx ; make docker-compose-build" > /dev/null
check_func $?

echo "[+] Fix bug compose with sed" ; 
ssh ${DEST} "cd awx ; sed -i 's/\$(COMPOSE_UP_OPTS) up/up \$(COMPOSE_UP_OPTS)/' Makefile"
check_func $?

echo "[+] Compose" ; 
ssh ${DEST} "cd awx ; make docker-compose COMPOSE_UP_OPTS=-d" > /dev/null
check_func $?

# UI
echo "[+] Build UI - about 5min" ; 
ssh ${DEST} "cd awx ; docker exec tools_awx_1 make clean-ui ui-devel" > /dev/null
check_func $?

# setup a TTY for docker over SSH
ssh -tt ${DEST} "cd awx ; docker exec -it tools_awx_1 awx-manage createsuperuser --username ${USERNAME} --email ${EMAIL} --noinput"
ssh -tt ${DEST} "cd awx ; docker exec -it tools_awx_1 awx-manage update_password --username=${USERNAME} --password=${PASSWORD}"
check_func $?

echo "====================================================="
echo "[+] - From your station, create a tunnel"
echo "ssh -L 8043:awx:8043 -p <EXT_PORT> cumulus@<WORKER>"
echo "====================================================="
sleep 5

echo "[+] - Setup container"
ssh ${DEST} "docker exec tools_awx_1 /bin/bash -c 'mkdir AWX-EE && cd AWX-EE && git clone -b ${AWX_EE_VERSION} https://github.com/ansible/awx-ee.git'"
scp execution_environment.yml awx:/tmp
scp requirements.yml awx:/tmp
scp requirements.txt awx:/tmp
scp hosts awx:/tmp

ssh ${DEST} "docker exec -i tools_awx_1 sh -c 'cat > AWX-EE/awx-ee/execution-environment.yml'" < execution_environment.yml
ssh ${DEST} "docker exec -i tools_awx_1 sh -c 'cat > AWX-EE/awx-ee/_build/requirements.yml'" < requirements.yml
ssh ${DEST} "docker exec -i tools_awx_1 sh -c 'cat > AWX-EE/awx-ee/_build/requirements.txt'" < requirements.txt

ssh ${DEST} "docker exec --user root -i tools_awx_1 bash -c 'cat >> /etc/hosts'" < hosts

echo "[+] Build AWX-EE - about 5min" ; 
ssh -tt ${DEST} "docker exec -it tools_awx_1 sh -c 'cd AWX-EE/awx-ee && tox -epodman'" > /dev/null
ssh -tt ${DEST} "docker exec -it tools_awx_1 sh -c 'cd AWX-EE/awx-ee && podman images ls'"

exit 
# EOF
